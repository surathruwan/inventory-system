 <?php  
 //insert_vehicle.php  
 include ('DataAccess/config.php');


 


 ?>
<!DOCTYPE html>  
 <html>  
      <head>  
           <title>Returned Item</title>  
           <link rel="stylesheet" href="css/bootstrap.min.css" />  
           <link rel="stylesheet" href="css/style.css" />
           <script src="js/bootstrap.min.js"></script> 
           <script src="http://parsleyjs.org/dist/parsley.js"></script> 
           <script src="js/jquery.min.js"></script>  
           <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
           <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
           <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
           <style type="text/css">


            .container{margin-top: 5%;}
            table tr:not(:first-child){
                cursor: pointer;transition: all .25s ease-in-out;
            }
            table tr:not(:first-child):hover{background-color: #ddd;}

           
           </style>
      </head>  
      <body>  

        <form id="main_issue_details">  
          <div class="col-md-12 ">
           
           <div class="col-md-4 ">
            <div class="form-group">
                     <label>Retuned Number</label>  
                     <input type="text" name="txt_issue_number" id="txt_issue_number"  class="form-control" readonly>
                    
                   </div>

           </div>

           <div class="col-md-4 ">
             <div class="form-group">
                      <label for="">Returned by</label>
                        <input type="text" name="txt_recieved_by" id="txt_recieved_by" class="form-control">
                    </div>
           </div>
         <!-- </div> -->

          <div class="col-md-4 ">
             <div class="form-group">
                      <label for="">Remark</label>
                        <input type="text" name="txt_project" id="txt_project" class="form-control">
                    </div>
           </div>
         </div>
       </form>
           <form id="add_return_item">
           <div class="col-md-12 ">

            <div class="col-md-1 ">
            <div class="form-group">
                     <label>Row</label>  
                     <input type="hidden" name="txt_issue_number_hidden" id="txt_issue_number_hidden" class="form-control"  readonly>
                     <input type="text" name="txt_issued_id" id="txt_issued_id" class="form-control"  readonly>
                    
                   </div>
           </div>
            
           <div class="col-md-3 ">
             <div class="form-group">
                      <label for="cmb_item_type">Item Type</label>
                      <select class="form-control" id="cmb_item_type" name="cmb_item_type"  ></select>
             </div>
           </div>
           <div class="col-md-3 ">
             <div class="form-group">
                      <label for="cmb_item_brand">Item Name</label>
                      <select class="form-control" id="cmb_item_brand" name="cmb_item_brand"  >
                      </select>
              </div>
           </div>
           <div class="col-md-3 ">
             <div class="form-group">
                      <label for="cmb_item_name">Item Name</label>
                      <!--   <input type="text" name="txt_item_name" id="txt_item_name" class="form-control"> -->
                       <select class="form-control" id="cmb_item_name" name="cmb_item_name"  >
                      </select>
                    </div>
           </div>
           <div class="col-md-2 ">
            <div class="form-group">
                     <label>Return Quantity</label>  
                     <input type="text" name="txt_quantity" id="txt_quantity" class="form-control">
                    
                   </div>
           </div>
           </form> 
                  <div class="col-md-12">
                   
                   
                    <input type="button"  style="float: right" name="cancel_update" id="cancel_update" class="btn btn-danger"  value="Cancel" />
                    <input type="button"  style="float: right;margin-right: 1%" name="update" id="update" class="btn btn-info" value="Add Return Queue" />
                   
                  </div>
                  
                
                
           </div> 

              <div class="col-md-12" style="height: 40px;padding-left: 30px">
                <div id="response"></div>  
              </div>
          

            <div class="col-md-12 content" >  <!-- begin of content div -->


    <!-- Search Bar -->
    <div class="col-md-12" style="padding: 0px">
      
       <div class="col-md-3">
        <label>Search By Date :</label> 
       <input type="text" id="datepicker" name="datepicker" class="form-control">
      </div>

      <div class="col-md-4">
        <label>Search By Issue Number :</label> 
        <select class="form-control" id="cmb_issue_number" name="cmb_issue_number"  ></select>
      </div>
      </div>



    <div class="col-md-12 table-view" style="width:100%;">
      <div class="col-md-5">
       <h2 class="heading_title" >Issued Items</h2>

        <hr>
       <div class="table-responsive">


        <table id="issued_data" class="table table-hover table-bordered">
          <thead style="background: #dbe5ee">
            <tr >
              
              <th data-column-id = "id">Item Count</th>
             <!--  <th data-column-id = "item_type">Item Type</th>
              <th data-column-id = "brand_name">Brand Name</th> -->
              <th data-column-id = "item_name">Item Name</th>
              <th data-column-id = "quantity">Quantity</th>
              <th hidden data-column-id = "id">Issue Number</th>
              <th></th>
            </tr>
          </thead>

          <tbody style="background:#fff" id="get_issued_details">
           
          </tbody>
        </table>
      </div>
       </div>


       <div class="col-md-2"></div>
       <div class="col-md-5">
         <h2 class="heading_title" >Returned Items</h2>


       <hr>
       <div class="table-responsive">


        <table id="issued_data" class="table table-hover table-bordered">
          <thead style="background: #dbe5ee">
            <tr >
              
              <th data-column-id = "id">Item Count</th>
             <!--  <th data-column-id = "item_type">Item Type</th>
              <th data-column-id = "brand_name">Brand Name</th> -->
              <th data-column-id = "item_name">Item Name</th>
              <th data-column-id = "quantity">Quantity</th>
              <th hidden data-column-id = "id">Issue Number</th>
              <th></th>
            </tr>
          </thead>

          <tbody style="background:#fff" id="get_returned_details">
           
          </tbody>
        </table>
      </div>


       </div>
    </div>
    <!-- <div class="col-md-1"></div> -->
  </div><!-- end of content div -->

</div>  <!-- end of main div -->
     
      </body>  
 </html>  


<!-- Get brand dependency -->
 <script type="text/javascript">

 $(document).ready(function() {
    $("#cancel_update").click(function(){
       cancel_update();
        
    }); 
});

  function cancel_update()
  {
     
      $('form').trigger("reset");  
      $('#cmb_item_brand').empty();
      $('#cmb_item_brand').append("<option>Loading......</option>");

      $('#cmb_item_name').empty();
      $('#cmb_item_name').append("<option>Loading......</option>");

       // $("#submit").show();
       $("#update").hide();
       $("#delete").hide();
       $("#cancel_update").hide();


  }
    
   
    $(document).ready(function(){

          IssueNumber();
          // $("#cmb_item_type").prop("disabled", true);
          // $("#cmb_item_brand").prop("disabled", true);
          // $("#cmb_item_name").prop("disabled", true);

          $('#cmb_item_brand').empty();
          $('#cmb_item_brand').append("<option>Loading......</option>");

          $('#cmb_item_type').change(function(){
            
          var type_id = $(this).val();
          var dataTosend='type='+type_id;
          get_brand_dependant(type_id);


    

      });
   });

    function get_brand_dependant(type_id,brand_id){
      $.ajax({

         url: 'admin/get_brand_dependant.php', //This is the current doc
         type: "POST",
         dataType:'json', // add json datatype to get json
         data: ({type_id: type_id}),
         success:function(data){
               console.log(data);
                // alert(data);
               $('#cmb_item_brand').empty();
               $('#cmb_item_brand').append("<option value='0'>-----Select Brand Name------</option>");
               $.each(data,function(i,item){
                //var brand_id = 1;
                
                  if(data[i].brand_id == brand_id)
                  {
                      $('#cmb_item_brand').append('<option value="'+ data[i].brand_id +'" selected >' + data[i].brandName + '</option>');
                  }else{
                  $('#cmb_item_brand').append('<option value="'+ data[i].brand_id +'">' + data[i].brandName + '</option>');
                }
               });
            },
        }); 
    }

 </script>

  <script type="text/javascript">
   
    function BrandItems(){
    $('#cmb_item_type').empty();
    $('#cmb_item_type').append("<option>Loading......</option>");

    $.ajax({
        type:"POST",
        url :"admin/get_item_types.php",
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success:function(data){
           $('#cmb_item_type').empty();
           $('#cmb_item_type').append("<option value='0'>-----Select Item Type------</option>");
           $.each(data,function(i,item){
              $('#cmb_item_type').append('<option value="'+ data[i].typeId +'">' + data[i].typeName + '</option>');
           });
        },
        complete:function(){

        }

    });
   }

   $(document).ready(function(){
      GetPreviousIssuedDetails();
     GetReturnDetails();
     $('#datepicker').datepicker
          ({
            dateFormat: 'yy-mm-dd',
          });
     $('form').trigger("reset");  
     $("#update").hide();
     $("#delete").hide();
     $("#cancel_update").hide();
      BrandItems();
      // BrandItems_tbl();
    

   });

 </script>



<!-- Begin Table -->
 <script type="text/javascript">
   
   
 
    $("#issued_data").on('click','tr:not(:first)',function() {
          
          $("#update").show();
          $("#delete").show();
          $("#cancel_update").show();
          var id = $(this).find("td:first-child").text();
          $("#txt_issued_id").val(id);
          $(this).toggleClass("select");
            $.ajax({
                url: 'admin/table/issued_details_history.php',
                method: 'POST',
                data: { id : id },
                success: function(data) {

                   var data = $.parseJSON(data);
                  
                   $("#cmb_item_type").val(data.item_type_id);
                   $("#cmb_item_name").val(data.item_name);
                   $("#cmb_item_brand").val(data.brand_brand_id);
                  
                    get_brand_dependant(data.item_type_id , data.brand_brand_id);
                    get_dependency_item(data.item_type_id,data.brand_brand_id,data.item_name);
                   // 
                   
                   // $("#txt_quantity").val(data.quantity);

                }
              });



        });


 </script>

 <!-- Get items according to the type and brand -->
 <script>

     $(document).ready(function(){
          $('#cmb_item_name').empty();
          $('#cmb_item_name').append("<option>Loading......</option>");

          $('#cmb_item_brand').change(function(){
             $('#cmb_item_name').empty();
          $('#cmb_item_name').append("<option>Loading......</option>");
          
          var type_id = $('#cmb_item_type').val();  
          
          var brand_id = $(this).val();
      
          var dataTosend='type_id='+type_id+'&brand_id='+brand_id;
          get_dependency_item(type_id,brand_id);
          
      });
   });


     function get_dependency_item(type_id,brand_id,item_id)
     {
           $.ajax({
         url: 'admin/get_item_dependant_on_type_brand.php', //This is the current doc
         type: "POST",
         dataType:'json', // add json datatype to get json
         data: ({type_id: type_id,brand_id: brand_id}),
         success:function(data){
               console.log(data);
                 
               $('#cmb_item_name').empty();
               $('#cmb_item_name').append("<option value='0'>-----Select Item------</option>");
               // alert(item_id);
               $.each(data,function(i,item){
                // alert(data[i].item_name);
                if( data[i].item_id == item_id )
                {
                  $('#cmb_item_name').append('<option value="'+ data[i].item_id +' " selected>' + data[i].item_name + '</option>');
                }
                else{
                  $('#cmb_item_name').append('<option value="'+ data[i].item_id +'">' + data[i].item_name + '</option>');
                }
               });

            },
        }); 
     }
 </script>

  


<!-- End Table -->

<!-- Get Details -->

 <script type="text/javascript">
   
    function GetPreviousIssuedDetails(){


     $('#cmb_issue_number').change(function() {
       var issue_number=$(this).val();
       var date=$('#datepicker').val();
       $('#get_issued_details').empty();
      $.ajax({
         url: 'admin/table/get_issue_details_according_date_and_number.php', //This is the current doc
         type: "POST",
         dataType:'json', // add json datatype to get json
         data: ({issue_number: issue_number , date:date}),
         success:function(data){
               var rows = '';
               $.each(data,function(i,item){
                
                   rows += '<tr>   <td> ' + data[i].id + ' </td>  <td> ' + data[i].item_name + ' </td> <td> ' + data[i].quantity + ' </td><td hidden> ' + data[i].issue_number + ' </td></tr>';

                   console.log(rows);

               });

               $('#get_issued_details').append(rows);
            },
        }); 

   });
   }

 </script>
<!-- End of Get Details -->


 <script type="text/javascript">
   
    function GetReturnDetails(){
   
    $.ajax({
        type:"POST",
        url :"admin/table/get_return_details.php",
        contentType:"application/json; charset=utf-8",
        dataType:"json",

        success:function(data){
           
           var rows = '';
            
           $.each(data,function(i,item){
            
               rows += '<tr>   <td> ' + data[i].id + ' </td>  <td> ' + data[i].item_name + ' </td> <td> ' + data[i].quantity + ' </td><td hidden> ' + data[i].issue_number + ' </td></tr>';

               console.log(rows);

           });
           $('#get_returned_details').append(rows);

        },
        complete:function(){

        }

    });
   }

 </script>


<!-- update item issue -->

  <script>  
 $(document).ready(function(){  
      $('#update').click(function(){  

           $('#update').prop('disabled', true);  
           var row          = $('#txt_issued_id').val();
           var item_type    = $('#cmb_item_type').val();  
           var item_brand   = $('#cmb_item_brand').val();  
           var item_name    = $('#cmb_item_name').val();  
           var quantity     = $('#txt_quantity').val();  
           
             
        
           if( row == '' || item_type == '0' || item_brand == '0' || item_name == '0' || quantity == ''  )  
           { 
         
                
                // $('#update').prop('disabled', false);  
                setTimeout(function(){  
                   $('#response').fadeOut("slow");  
                 }, 3000);  
                $('#response').html('<span class="text-danger">All Fields are required</span>')  
           }  
           else  
           {  
              if(confirm("Are you sure you want to add this?")){
                $.post(  

                     'admin/add_return_item.php',  
                     $('#add_return_item').serialize(),  

                     function(data)  
                     {    
                           $('#get_issued_details').empty();
                         GetPreviousIssuedDetails();
                          $('#get_returned_details').empty();
                           GetReturnDetails();
                          $('form').trigger("reset");  
                          // cancel_update();
                         
                          $("#update").hide();
                          $("#cancel_update").hide();
                          $('#response').fadeIn().html(data);  
                          $('#update').prop("disabled", false);
                           
                          setTimeout(function(){  
                               $('#response').fadeOut("slow");  
                          }, 5000);  
                     }  
                );  
              }

               // GetReturnDetails();
           }  
      }); 
 });


 </script> 


 <!-- update item issue -->

 
 <!-- End of Item Delete Queue -->
   <script>
 $('#txtReportdate').datepicker
    ({
        dateFormat: 'yy-mm-dd',
        });
  </script>



  <!-- Transfer Item to the order History Table and Main Data add to the issue_order table -->
  <script>  
 $(document).ready(function(){  
      $('#order_complete').click(function(){
        setMainIssueData();
       

    $.ajax({
        type:"POST",
        url :"admin/table/transfer_issue_details.php",
        contentType:"application/json; charset=utf-8",
        dataType:"json",
       
        success:function(data){
        //    console.log(data);
        // alert('issueNumber');
           
        },
        complete:function(){
          
        }

       });

      

       
      });  

      
 });

// Insert main details to the issue_main table
function setMainIssueData()
{
           var issueNumber  = $('#txt_issue_number').val();  
           var recievedBy   = $('#txt_recieved_by').val();  
           var project      = $('#txt_project').val();  
           
           
             
        
           if( issueNumber == '' || recievedBy == '' || project == ''  )  
           {  
         
                
                $('#response').html('<span class="text-danger">All Fields are required</span>')  
           }  
           else  
           {  
            
                $.post(  

                     'admin/insert_main_details.php ',  
                     $('#main_issue_details').serialize(),  

                     function(data)  
                     {    
                          
                          
                          IssueNumber();

                          $('#main_issue_details').trigger("reset");  
                          $('#response').fadeIn().html(data);  
                          // $('#order_complete').prop("disabled", false);
                           
                          setTimeout(function(){  
                               $('#response').fadeOut("slow");  
                          }, 5000);  
                     }  
                );
                $('#get_issued_details').empty(); // For empty the previous item List  

           } 

}

// Generate Issue Number
function IssueNumber(){
   $.ajax({
        type:"POST",
        url :"admin/get_issue_number.php",
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success:function(data){
          
           $.each(data,function(i,item){
               $('#txt_issue_number').val(data[i].GenerateIssuedNumber);
               $('#txt_issue_number_hidden').val(data[i].GenerateIssuedNumber);
               
           });
        },
        complete:function(){

        }

    });
   }




 </script> 


<script type="text/javascript">
   
   
 
    $(document).ready(function(){
          $('#cmb_issue_number').empty();
          $('#cmb_issue_number').append("<option>Loading......</option>");

          $('#datepicker').change(function(){

            var date = $(this).val();
            alert(date);
            
          // var type_id = $(this).val();
          // var dataTosend='type='+type_id;


    $.ajax({
         url: 'admin/get_issued_number_search_by_date.php', //This is the current doc
         type: "POST",
         dataType:'json', // add json datatype to get json
         data: ({date: date}),
         success:function(data){
               console.log(data);
                // alert(data);
               $('#cmb_issue_number').empty();
               $('#cmb_issue_number').append("<option value='0'>-----Select Issue Number------</option>");
               $.each(data,function(i,item){
                  $('#cmb_issue_number').append('<option value="'+ data[i].issue_number +'">' + data[i].issue_number + '</option>');
               });
            },
        }); 

      });
   });
 </script>