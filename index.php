<!DOCTYPE html>  
 <html>  
      <head>  
           <title>Add Item</title>  
           <link rel="stylesheet" href="css/bootstrap.min.css" />  
           <link rel="stylesheet" href="css/style.css" />
           <script src="js/bootstrap.min.js"></script> 
           <script src="http://parsleyjs.org/dist/parsley.js"></script> 
           <script src="js/jquery.min.js"></script>  
           <style type="text/css">

            .container{margin-top: 5%;}
           
           </style>
      </head>  
      <body>  
           
           <div class="container box">
           
           <div class="col-md-1"></div>
            <div class="col-md-10 box">
                <h3>Add Item</h3> 
                
                <form id="add_item">  

                  <div class="form-group">
                      <label for="cmb_item_type">Item Type</label>
                      <select class="form-control" id="cmb_item_type" name="cmb_item_type"  ></select>
                   </div>

                    <div class="form-group">
                      <label for="cmb_item_brand">Brand Name</label>
                      <select class="form-control" id="cmb_item_brand" name="cmb_item_brand"  >
                      </select>
                    </div>

                   <div class="form-group">
                      <label for="txt_item_name">Item Name</label>
                        <input type="text" name="txt_item_name" id="txt_item_name" class="form-control">
                    </div>

                    
                   <div class="form-group">
                     <label>Quantity</label>  
                     <input type="text" name="txt_quantity" id="txt_quantity" class="form-control">
                     </select>
                   </div>

                   <div class="form-group">
                     <label>Serial Number</label> 
                      <input type="text" name="txt_serial" id="txt_serial" class="form-control">
                   </div>

                     <input type="button"  name="submit" id="submit" class="btn btn-info" value="Add Item" />  
                </form>  

                <div id="response"></div>
                </div>
           </div> 
           <div class="col-md-1"></div>
           </div> 
      </body>  
 </html>  



 <script type="text/javascript">
   
   
 
    $(document).ready(function(){
          $('#cmb_item_brand').empty();
          $('#cmb_item_brand').append("<option>Loading......</option>");

          $('#cmb_item_type').change(function(){
            
          var type_id = $(this).val();
          var dataTosend='type='+type_id;


    $.ajax({
         url: 'admin/get_brand_dependant.php', //This is the current doc
         type: "POST",
         dataType:'json', // add json datatype to get json
         data: ({type_id: type_id}),
         success:function(data){
               console.log(data);
                // alert(data);
               $('#cmb_item_brand').empty();
               $('#cmb_item_brand').append("<option value='0'>-----Select Brand Name------</option>");
               $.each(data,function(i,item){
                  $('#cmb_item_brand').append('<option value="'+ data[i].brand_id +'">' + data[i].brandName + '</option>');
               });
            },
        }); 

      });
   });

//////
    // Load Model Name
   


    // for (i = new Date().getFullYear(); i > 1985; i--)
    // {
    //   $('#cmb_car_year').append($('<option />').val(i).html(i));
    // }


 </script>

  <script type="text/javascript">
   
    function bodyTypes(){
    $('#cmb_item_type').empty();
    $('#cmb_item_type').append("<option>Loading......</option>");

    $.ajax({
        type:"POST",
        url :"admin/get_item_types.php",
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success:function(data){
           $('#cmb_item_type').empty();
           $('#cmb_item_type').append("<option value='0'>-----Select Item Type------</option>");
           $.each(data,function(i,item){
              $('#cmb_item_type').append('<option value="'+ data[i].typeId +'">' + data[i].typeName + '</option>');
           });
        },
        complete:function(){

        }

    });
   }

   $(document).ready(function(){
      bodyTypes();
   });

 </script>



 <!-- Insert -->
  <script>  
 $(document).ready(function(){  
      $('#submit').click(function(){  

           $('#submit').prop('disabled', true);  
           var item_type    = $('#cmb_item_type').val();  
           var item_brand   = $('#cmb_item_brand').val();  
           var item_name    = $('#txt_item_name').val();  
           var quantity     = $('#txt_quantity').val();  
           var Serial       = $('#txt_serial').val();  
             
        
           if( item_type == '0' || item_brand == '0' || item_name == '' || quantity == '' || Serial == '' )  
           {  
         
                
                $('#submit').prop('disabled', false);  
                setTimeout(function(){  
                   $('#response').fadeOut("slow");  
                 }, 3000);  
                $('#response').html('<span class="text-danger">All Fields are required</span>')  
           }  
           else  
           {  
            
                $.post(  

                     'admin/insert_item.php ',  
                     $('#add_item').serialize(),  
                     function(data)  
                     {    
                          $('form').trigger("reset");  
                          $('#response').fadeIn().html(data);  
                          $('#submit').prop("disabled", false);
                           
                          setTimeout(function(){  
                               $('#response').fadeOut("slow");  
                          }, 5000);  
                     }  
                );  
           }  
      });  
 });


 </script> 

