<?php  
 include ('DataAccess/config.php');
 // $connect = mysqli_connect("localhost", "root", "", "testing");  
 $query ="SELECT it.typeName AS type_name , b.brandName AS brand_name ,i.item_name AS item_name ,i.quantity AS quantity, i.serial_number AS serial_number from  item_type it , brands b ,items i where it.typeId = i.typeId and b.brand_id = i.brand_brandId ";  
 $result = mysqli_query($connect, $query);  
 ?>  
 <!DOCTYPE html>  
 <html>  
      <head>  
           <title>Item Details</title>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>          
           <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bootgrid/1.3.1/jquery.bootgrid.min.js"></script>
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-bootgrid/1.3.1/jquery.bootgrid.css" />  
      </head>  
      <body>  
           <br /><br />  
           <div class="container">  
                <h3 align="center">Item Details</h3>  
                <br />  
                
                <div class="table-responsive">  
                     <table id="items_data" class="table table-striped table-bordered">  
                          <thead>  
                               <tr>  
                               		  
                                    <th data-column-id="type_name" >Type Name</th>  
                                    <th data-column-id="brand_name">Brand Name</th>  
                                    <th data-column-id="item_name">Item Name</th>  
                                    <th data-column-id="quantity">Quantity</th> 
                                    <th data-column-id="serial_number">Serial Number</th>
                                    
                               </tr>  
                          </thead>  
                          <tbody>  
                          <?php  
                          while($row = mysqli_fetch_array($result))  
                          {  
                               echo '  
                               <tr> 
                               	
                                    <td>'.$row["type_name"].'</td>  
                                    <td>'.$row["brand_name"].'</td>  
                                    <td>'.$row["item_name"].'</td>  
                                    <td>'.$row["quantity"].'</td> 
                                    <td>'.$row["serial_number"].'</td>  
                                     
                                     
                               </tr>  
                               ';  
                          }  
                          ?>  
                          </tbody>  
                     </table>  
                </div>  
           </div>  
      </body>  
 </html>  
 <script>  
 $("#items_data").bootgrid();  
 </script>  

 