<?php 
	
	include ('../DataAccess/config.php');
	
	$sql = "Select brandId,brand_name from car_brands"; 
	
	// Execute the query and store the result set 
	$result = mysqli_query($connect, $sql); 
	
	if (mysqli_num_rows($result) > 0)
	{ 
		$data = array();
   		while($row=mysqli_fetch_array($result)){
   			$data[] = array(
   				'id' => $row['brandId'],
   				'brand_name' => $row['brand_name']
   			);
   		}

   		header('Content-type:application/json');
   		echo json_encode($data);
	} 

	// connect close 
	mysqli_close($connect); 
?> 
