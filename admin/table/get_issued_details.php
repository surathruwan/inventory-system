<?php 
	
	include ('../../DataAccess/config.php');
	
	$sql = "SELECT temp_i.id,it.typeId,it.typeName,b.brand_id,b.brandName,i.item_name,temp_i.quantity,temp_i.issue_number FROM temp_issued_items temp_i , item_type it , brands b ,items i where temp_i.item_type_id = it.typeId and temp_i.brand_brand_id = b.brand_id and temp_i.item_name = i.item_Id"; 
	
	// Execute the query and store the result set 
	$result = mysqli_query($connect, $sql); 
	
	if (mysqli_num_rows($result) > 0)
	{ 
		$data = array();
   		while($row=mysqli_fetch_array($result)){
   			$data[] = array(
   				'id' => $row['id'],
   				'typeId' => $row['typeId'],
   				'typeName' => $row['typeName'],
   				'brand_id' => $row['brand_id'],
   				'brandName' => $row['brandName'],
   				'item_name' => $row['item_name'],
   				'quantity' => $row['quantity'],
   				'issue_number' => $row['issue_number']
   			);
   		}

   		header('Content-type:application/json');
   		echo json_encode($data);
	} 

	// connect close 
	mysqli_close($connect); 
?> 