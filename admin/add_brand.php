<!DOCTYPE html>  
 <html>  
      <head>  
           <title>Add Brand</title>  
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
      </head>  
      <body>  
           <br /><br /> 
           <div class="container">
           
           <div class="col-md-2"></div>
            <div class="col-md-8">
                <h3>Add Brand</h3><br />  
                <form id="brand_form">  
                   <div class="form-group">
                     <label>Item Type</label>  
                     <select class="form-control" id="cmb_item_type" name="cmb_item_type"> 
                        <option value="0">---Select Item Type---</option>
                        <option value="1">Tools</option>
                        <option value="2">Accessories</option>
                      </select>
                    </div>

                     <div class="form-group">
                      <label for="cmb_item_brand">Brand Name</label>
                      <input type="text" name="txt_item_brand" id="txt_item_brand" class="form-control">
                    </div>
                     <input type="button" name="submit" id="submit" class="btn btn-info" value="Add Brand" />  
                </form>  
                <div id="response"></div>  
           </div> 
           <div class="col-md-2"></div>
           </div> 
      </body>  
 </html>  
 <script>  
 $(document).ready(function(){  
      $('#submit').click(function(){  
           $('#submit').prop('disabled', true);  
           var item_type = $('#cmb_item_type').val();  
           var brand     = $('#txt_item_brand').val();  
           
        
           if(item_type == '0' || brand == '')  
           {  
                $('#response').html('<span class="text-danger">All Fields are required</span>')  
                $('#submit').prop('disabled', false);  
           }  
           else  
           {  
            
                $.post(  

                     'insert_brand.php ',  
                     $('#brand_form').serialize(),  
                     function(data)  
                     {    
                          $('form').trigger("reset");  
                          $('#response').fadeIn().html(data);  
                          $('#submit').prop("disabled", false);
                           
                          setTimeout(function(){  
                               $('#response').fadeOut("slow");  
                          }, 5000);  
                     }    
                );  
           }  
      });  
 });  
 </script>  

